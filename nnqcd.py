#!/usr/bin/env python

"""
Neural network quantum state as an ansatz for lattice QCD.
"""

import argparse
from dataclasses import dataclass
import functools
import itertools
import os
import pickle
import sys

import equinox as eqx
from equinox import nn
import jax
import jax.numpy as jnp
import numpy as np
import optax
jax.config.update('jax_platform_name', 'cpu')

@dataclass
class Lattice:
    """ Lattice geometry.

    Args:
        L: length of one side of the lattice
        D: number of spacetime dimensions
    """
    L: int
    D: int = 3

    def volume(self):
        """ Returns the total number of sites in the lattice.
        """
        return self.L**self.D

    def coord(self, site, mu):
        """ Compute the coordinates of a site.

        Args:
            site: The index of the site in question.
            mu: The coordinate to compute.

        Returns:
            The `mu` coordinate of the specified site.

        Raises:
            ValueError: if `mu` isn't a valid direciton
        """

        if not mu < self.D:
            raise ValueError(f'invalid direction {mu}')

        return (site%(self.L**(mu+1))) // (self.L**mu)

    def step(self, site, mu, dist):
        """ Compute the index of one site, from a starting site and an offset.

        Args: 
            site: The site to start from.
            mu: The direction to walk in.
            dist: Number of steps (in the positive direction) to walk.

        Returns:
            The index of the specified site.

        Raises:
            ValueError: if `mu` isn't a valid direction.
        """

        if not mu < self.D:
            raise ValueError(f'invalid direction {mu}')

        # First identify the component of `site` in the `mu` direction.
        imu = self.coord(site, mu)

        # The index of the site that is `0` in the `mu` direction.
        base = site - (imu * self.L**mu)

        # Shift imu
        imu += dist
        imu %= self.L

        return base + (imu * self.L**mu)

    def flat(self, N):
        """ Produce a flat gauge configuration.

        Args:
            N: number of colors

        Returns:
            A rank-4 array containing a gauge configuration consisting only of
            the identity.
        """
        return jnp.tile(jnp.eye(N),(self.volume(),self.D,1,1))

class SpecialUnitaryAlgebra:
    """ The su(N) Lie algebra.

    The basis is orthonormalized (so the normalization differs from that of the
    Pauli and Gell-Mann bases).
    """

    def __init__(self, N):
        """
        Args:
            N: number of colors
        """
        self.N = N
        self.dimension = N**2-1
        self.basis = np.zeros((N**2-1,N,N))+0j

        # The first N*(N-1)/2 components specify the real part of the triangle. The
        # second N*(N-1)/2 components specify the imaginary part of the same
        # triangle. That leaves (N-1) components, which yield the diagonal.
        basis_r = self.basis[:(N*(N-1))//2]
        basis_i = self.basis[(N*(N-1))//2:N*(N-1)]
        basis_d = self.basis[N*(N-1):]

        # Triangular parts.
        triu = np.triu_indices(N, 1)
        for i in range((N*(N-1))//2):
            # Real (symmetric)
            basis_r[i,triu[0][i],triu[1][i]] = 1./np.sqrt(2)
            basis_r[i,triu[1][i],triu[0][i]] = 1./np.sqrt(2)
            # Imaginary (antisymmetric)
            basis_i[i,triu[0][i],triu[1][i]] = 1.j/np.sqrt(2)
            basis_i[i,triu[1][i],triu[0][i]] = -1.j/np.sqrt(2)

        # Diagonal parts.
        for i in range(N-1):
            d = np.zeros((N,))
            d[:i+1] = 1.
            d[i+1] = -(i+1)
            d /= np.sqrt((i+2)*(i+1))
            basis_d[i][np.diag_indices(N)] = d

        # Compute structure constants
        self.structure = np.zeros((self.dimension, self.dimension, self.dimension))+0j
        for a,Ta in enumerate(self.basis):
            for b,Tb in enumerate(self.basis):
                # Compute commutator
                Ta, Tb = self.basis[a], self.basis[b]
                H = Ta@Tb - Tb@Ta
                for c,Tc in enumerate(self.basis):
                    self.structure[a,b,c] = np.trace(H@Tc)

    def vector(self, H):
        """
        Obtain a vector from an element of the Lie algebra.

        Args:
            H: A traceless Hermitian N-by-N matrix.

        Returns:
            A vector.
        """
        idx = jnp.arange(self.dimension)
        def vi(i):
            return jnp.trace(H@jnp.array(self.basis)[i])
        return jax.vmap(vi)(idx)

    def __call__(self, v):
        """
        Obtain an element of the Lie algebra from a flat vector.

        Args:
            v: A vector of size (N**2-1)

        Returns:
            A traceless Hermitian N-by-N matrix.
        """
        return jnp.einsum('i,iab->ab', v, self.basis)

def random_hermitian(key, N, sigma, traceless=False):
    """ Generates a random Hermitian matrix.

    Args:
        key: A PRNG key to consume.
        N: dimension of the matrix
        sigma: standard deviation
        traceless: If `True` (the default), the traceless part is returned

    Returns:
        An N-by-N Hermitian matrix.

    Raises:
        ValueError: If `sigma` is not a non-negative real number.
    """

    key_real, key_imag = jax.random.split(key)
    HR = jax.random.normal(key_real, shape=(N,N))*sigma
    HI = jax.random.normal(key_imag, shape=(N,N))*sigma
    H = HR + 1j*HI
    H += H.conj().transpose()
    H /= 2.

    if traceless:
        H -= jnp.eye(N)*H.trace()

    return H

def random_unitary(key, N, sigma, special=True):
    """ Generates a random unitary matrix.

    Args:
        key: A PRNG key to consume.
        N: dimension of group
        sigma: inverse concentration near the identity
        special: If `True` (the default), the determinant is constrained to be
            1.

    Returns:
        A matrix in the group U(N) or SU(N).

    Raises:
        ValueError: If `sigma` is not a non-negative real number.
    """

    H = random_hermitian(key, N, sigma, traceless=special)
    Hvals, Hvecs = jnp.linalg.eigh(H)
    return Hvecs @ jnp.diag(jnp.exp(-1j*Hvals)) @ Hvecs.conj().transpose()

def bootstrap(key, xs, ws=None, N=100, Bs=50):
    """ Compute bootstrapped error bars.

    If `xs` is complex, then the real part of the returned error represents the
    error on the real part of the mean, and ditto for the imaginary part.

    Args:
        key: Key for resampling
        xs: Samples.
        ws: Weights.
        N: Number of resamplings to perform.
        Bs: Number of blocks to use.

    Returns:
        An ordered pair consisting of the reweighted mean and its estimated
        standard deviation.
    """
    if Bs > len(xs):
        Bs = len(xs)
    B = len(xs)//Bs
    if ws is None:
        ws = xs*0 + 1
    # Block. First, truncate any samples that don't fit in our block structure.
    xs = xs[:Bs*B]
    ws = ws[:Bs*B]
    # Reshape, so that the first index specifies the block.
    xs = xs.reshape((Bs,B))
    ws = ws.reshape((Bs,B))
    # Get per-block weights and means.
    w = jnp.sum(ws, axis=0)
    x = jnp.sum(xs*ws, axis=0)/w
    # Regular bootstrap
    y = x * w
    m = (sum(y) / sum(w))
    keys = jax.random.split(key, N)
    def resampled_mean(k):
        s = jax.random.choice(k, jnp.arange(len(x)), (len(x),))
        return sum(y[s]) / sum(w[s])
    ms = jax.vmap(resampled_mean)(keys)
    return m, np.std(ms.real) + 1j*np.std(ms.imag)

class Metropolis:
    """ Markov chain using the Metropolis algorithm.
    """

    def __init__(self, x0, wf, propose, key):
        """
        Args:
            x0: Initial field configuration.
            wf: The wavefunction.
            propose: A function for generating proposals.
            key: PRNG key for generating proposals.
        """
        self.x = x0
        self.wf = wf
        self.propose = propose
        self.delta = 1.
        self._key = key
        self._recent = [False]

    def step(self, N=1):
        """
        Args:
            N: The number of steps to take before returning a configuration.
        """
        self.P = np.abs(self.wf(self.x))**2
        for _ in range(N):
            kprop, kacc, self._key = jax.random.split(self._key, 3)
            xp = self.propose(kprop, self.x, self.delta)
            Pp = np.abs(self.wf(xp))**2
            Pacc = Pp / self.P
            accepted = False
            if jax.random.uniform(kacc) < Pacc:
                self.x = xp
                self.P = Pp
                accepted = True
            self._recent.append(accepted)
        self._recent = self._recent[-100:]

    def calibrate(self, skip=100):
        """ Calibrate the chain, tweaking `delta` until the acceptance rate
        lies between 0.2 and 0.8.
        """
        self.step(N=skip)
        ar_lo, ar_hi = 0.2, 0.8
        while self.acceptance_rate() < ar_lo or self.acceptance_rate() > ar_hi:
            if self.acceptance_rate() < ar_lo:
                self.delta *= 0.9
            if self.acceptance_rate() > ar_hi:
                self.delta *= 1.1
            self.step(N=skip)

    def acceptance_rate(self):
        """ Estimates the acceptance rate.

        Returns:
            The acceptance rate over the last 100 steps.
        """
        return sum(self._recent) / len(self._recent)

    def iter(self, skip=1):
        while True:
            self.step(N=skip)
            yield self.x

class Wavefunction(eqx.Module):
    layers: list

    def __init__(self, key, L, N=3):
        # Number of physical degrees of freedom
        dof_gauge = L**3 * 3 * N * N * 2
        dof_fermi = L**3 * N
        dof = dof_gauge + dof_fermi
        width = [dof, 2*dof, 2]
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n]) for n in range(Nlayers)]

    @jax.jit
    def __call__(self, U, n):
        """
        Args:
            U: Links. Shape is (L**3, 3, N, N).
            n: Fermion occupation numbers. Shape is (L**3, N).
        """
        U = U.ravel()
        n = n.ravel()
        x = jnp.concatenate([U.real, U.imag, n])
        for layer in self.layers[:-1]:
            x = jax.nn.celu(layer(x))
        psi_ri = self.layers[-1](x)
        return psi_ri[0] + 1j*psi_ri[1]

class ConvolutionalWavefunction(eqx.Module):
    pass

class InvariantWavefunction(eqx.Module):
    """ A gauge-invariant wavefunction ansatz.
    """
    layers: list

    def __init__(self, key, L, N=3):
        ins = len(self._gi(jnp.zeros((L**3,3,N,N)), jnp.zeros((L**3,N))))
        width = [ins, 3*ins, 2]
        Nlayers = len(width)-1
        keys = jax.random.split(key, Nlayers)
        self.layers = [nn.Linear(width[n], width[n+1], key=keys[n]) for n in range(Nlayers)]

    def _gi(self, U, n):
        """ Compute a list of gauge-invariant quantities.

        Args:
            U: Links. Shape is (L**3, 3, N, N).
            n: Fermion occupation numbers. Shape is (L**3, N).
        """
        # TODO

    @jax.jit
    def __call__(self, U, n):
        """
        Args:
            U: Links. Shape is (L**3, 3, N, N).
            n: Fermion occupation numbers. Shape is (L**3, N).
        """
        x = self._gi(U, n)
        for layer in self.layers[:-1]:
            x = jax.nn.celu(layer(x))
        psi_ri = self.layers[-1](x)
        return psi_ri[0] + 1j*psi_ri[1]

def probability(wf, U, n):
    """
    Args:
        wf:
        U:
        n:

    Returns:
    """
    psi = wf(U, n)
    return jnp.abs(psi)**2

def hamiltonian_stochastic(key):
    """
    Stochastic estimator of (H psi) at the given field configuration.

    Args:
        key:
        wf:
        L:
        N:
        g:
        U:
        n:

    Returns:
    """

def hamiltonian(wf, L, N, g, U, n):
    """
    Exactly evaluate (H psi) at the given field configuration.

    Args:
        wf:
        L:
        N:
        g:
        U:
        n:

    Returns:
    """
    assert U.shape == (L**3, 3, N, N)

    psi = wf(U, n)
    Hpsi = 0.

    lattice = Lattice(L)
    algebra = SpecialUnitaryAlgebra(N)

    rs = jnp.arange(lattice.volume())

    # Gauge kinetic
    gk_coef = 4 * g**2
    def wf_(x):
        H = jax.vmap(algebra)(x.reshape((lattice.D*lattice.volume(),algebra.dimension)))
        H = H.reshape((lattice.volume(),lattice.D,N,N))
        V = U + 1j*U@H@U
        return wf(V, n)
    def lap_part(i):
        def wf_i(x):
            z = jnp.zeros(lattice.D*lattice.volume()*algebra.dimension).at[i].set(x)
            return wf_(z)
        _real = jax.grad(jax.grad(lambda x: wf_i(x).real))(0.)
        _imag = jax.grad(jax.grad(lambda x: wf_i(x).imag))(0.)
        return _real + 1j*_imag
    #Hpsi -= gk_coef * jnp.sum(jax.vmap(lap_part)(jnp.arange(lattice.D*lattice.volume()*algebra.dimension)))
    Hpsi -= gk_coef * jnp.sum(jax.lax.map(lap_part,jnp.arange(lattice.D*lattice.volume()*algebra.dimension)))

    # Gauge potential
    gp_coef = 1/g**2
    def plaquette(r,i,j):
        # Shifted sites
        r_i = lattice.step(r,i,1)
        r_j = lattice.step(r,j,1)

        # Involved links
        U1 = U[r,i,:,:]
        U2 = U[r_i,j,:,:]
        U3 = U[r_j,i,:,:].conj().transpose()
        U4 = U[r,j,:,:].conj().transpose()

        # Product
        return U4 @ U3 @ U2 @ U1
    def plaquette_potential(r):
        ret = 0.
        for i,j in [(0,1), (0,2), (1,2)]:
            V = plaquette(r,i,j)
            ret += N - jnp.trace(V).real
        return ret
    Hpsi += gp_coef * jnp.sum(jax.vmap(plaquette_potential)(rs)) * psi

    # Fermion hopping
    fk_coef = 1.
    # TODO

    # Gauge penalty
    if not isinstance(wf, InvariantWavefunction):
        # TODO
        pass

    return (psi.conj() * Hpsi).real

@eqx.filter_jit
@functools.partial(eqx.filter_value_and_grad, has_aux=True)
def loss(wf, key, L, N, g, U, n, psamp):
    """
    Computes the expectation value of the Hamiltonian.

    Args:
        wf: The wavefunction.
        key: Key for bootstrapping.
        L: Lattice size.
        N: Number of colors.
        g: Gauge coupling.
        U: Sampled links.
        n: Sampled occupation numbers.
        psamp: Sampled probabilities.
    """
    p = jax.vmap(probability, in_axes=(None, 0, 0))(wf, U, n)
    h = jax.vmap(hamiltonian, in_axes=(None, None, None, None, 0, 0))(wf, L, N, g, U, n)
    assert p.shape == psamp.shape
    w = p/psamp
    ham, herr = bootstrap(key, h, w)
    return ham, herr

def train(args):
    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()
    wfKey, mcKey, lsKey = jax.random.split(jax.random.PRNGKey(seed), 3)

    # Parameters to train on.
    nums = (np.linspace(0, args.density, args.chains)*args.L**3).astype(int)

    # Chain keys.
    chainKeys = jax.random.split(mcKey, len(nums))

    # Load or initialize wavefunction.
    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')
    loaded = False
    if not args.init:
        # Attempt to load.
        if os.path.isdir(args.model):
            with open(modelFilename, 'rb') as f:
                L, g, Wf = pickle.load(f)
                assert L == args.L
                assert g == args.g
            wf = Wf(wfKey, args.L)
            eqx.tree_deserialise_leaves(wfEqxFilename, wf)
            loaded = True
    if not loaded:
        # Create new model.
        try:
            os.makedirs(args.model)
        except FileExistsError:
            pass
        wf = Wavefunction(wfKey, args.L)
        with open(modelFilename, 'wb') as f:
            pickle.dump((args.L, args.g, Wavefunction), f)
    def save():
        eqx.tree_serialise_leaves(wfEqxFilename, wf)

    sched = optax.warmup_exponential_decay_schedule(
            init_value=1e-4,
            peak_value=1e-2,
            warmup_steps=1000,
            transition_steps=2000,
            decay_rate=0.1,
            end_value=2e-5)
    opt = getattr(optax,args.optimizer)(sched)
    opt_state = opt.init(wf)

    # Sampling parameters
    NSAMPLES = 10
    NSKIP = 10

    # Train.
    lattice = Lattice(args.L)
    try:
        # Initial configuration.
        U0 = lattice.flat(3)

        # Gauge proposal
        @jax.jit
        def propose_gauge(key, x, delta):
            delta /= jnp.sqrt(args.L**3)
            U, n = x

            # Alter gauge configuration (according to delta).
            gkeys = jax.random.split(key, 3*args.L**3)
            V = jax.vmap(lambda k: random_unitary(k, 3, delta))(gkeys)
            V = V.reshape(U.shape)
            U_ = jnp.einsum('xiab,xibc->xiac', U, V)

            return U_, n

        def propose_fermions(key, x):
            U, n = x

            # TODO
            return U, n

        def propose(key, x, delta):
            ckey, key = jax.random.split(key)
            if jax.random.uniform(ckey) < 0.5:
                return propose_gauge(key, x, delta)
            else:
                return propose_fermions(key, x)

        # Construct chains for various sets of parameters.
        def make_chain(num, key):
            n0 = np.zeros((args.L**3,3))
            n0[:num,:] = 1.
            return Metropolis((U0, n0), lambda x: wf(*x), propose, key)

        chains = [make_chain(num, key) for num, key in zip(nums, chainKeys)]
        while True:
            Us = []
            ns = []
            ps = []

            # Sample
            for i, chain in enumerate(chains):
                if args.verbose:
                    print(f"Calibrating (chain {i}/{len(chains)})...")
                chain.calibrate(skip=NSKIP)
                if args.verbose:
                    print(f"Sampling (chain {i}/{len(chains)})...")
                for _ in range(NSAMPLES):
                    chain.step(NSKIP)
                    U, n = chain.x
                    Us.append(U)
                    ns.append(n)
                    ps.append(chain.P)

            # Train until the samples are no longer good.
            if args.verbose:
                print("Training...")
            U = jnp.array(Us)
            n = jnp.array(ns)
            p = jnp.array(ps)
            def fresh(p, psamp):
                q = np.std(p/psamp)/np.mean(p/psamp)
                return q < 0.2
            while fresh(jax.vmap(probability, in_axes=(None, 0, 0))(wf, U, n), p):
                (loss_val, loss_err), loss_grad = loss(wf, lsKey, args.L, 3, args.g, U, n, p)
                updates, opt_state = jax.jit(opt.update)(loss_grad, opt_state)
                wf = eqx.filter_jit(eqx.apply_updates)(wf, updates)
                if args.verbose:
                    print(f"Stepped {loss_val} {loss_err}")
                save()
    except KeyboardInterrupt:
        save()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Train ground state")
    parser.add_argument('-c', '--chains', type=int, default=10,
            help="number of Markov chains to run")
    parser.add_argument('-i', '--init', action='store_true',
            help="initialize anew, rather than load")
    parser.add_argument('-n', '--density', type=float, default=0.1,
            help="maximum lattice density")
    parser.add_argument('-v', '--verbose', action='store_true',
            help="verbose output")
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")

    parser.add_argument('model', type=str, help='model and wavefunction directory')
    parser.add_argument('L', type=int, help='lattice size')
    parser.add_argument('g', type=float, help='gauge coupling')

    parser.add_argument('-o', '--optimizer', choices=['adam','sgd','yogi'], default='adam', help='optimizer to use')

    args = parser.parse_args()
    train(args)

