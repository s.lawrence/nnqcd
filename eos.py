#!/usr/bin/env python

import argparse

import jax

from nnqcd import *

def eos(args):
    seed = args.seed
    if args.seed_time:
        seed = time.time_ns()

    modelFilename = os.path.join(args.model, 'model.pickle')
    wfEqxFilename = os.path.join(args.model, 'wf.eqx')
    with open(modelFilename, 'rb') as f:
        L, g, Wf = pickle.load(f)
    wf = Wf(jax.random.PRNGKey(0), L)
    eqx.tree_deserialise_leaves(wfEqxFilename, wf)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Train ground state")
    parser.add_argument('--seed', type=int, default=0,
            help="random seed")
    parser.add_argument('--seed-time', action='store_true',
            help="seed PRNG with current time")
    parser.add_argument('model', type=str, help='model and wavefunction directory')
    args = parser.parse_args()
    eos(args)

