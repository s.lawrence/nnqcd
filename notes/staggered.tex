\documentclass[letterpaper]{amsart}
\usepackage{xcolor}
\usepackage{slashed}

\newcommand{\todo}{\colorbox{pink}{\textsc{Todo}}}
\begin{document}
\title{Staggered Fermions}
\author{Scott Lawrence}
\begin{abstract}
These notes describe staggered fermions in $3+1$ dimensions, in the Hamiltonian formalism, with an eye towards studying Hamiltonian lattice QCD.
\end{abstract}
\maketitle

\section{One dimension}

Our target theory is free fermions in the one-dimensional continuum. This system is described by the Hamiltonian density
\begin{equation}
\mathcal H =
\bar\psi (i \gamma^i\partial_i + m) \psi
\text.
\end{equation}
Here $\psi$ is a two-component spinor, and we adopt the shorthand $\bar\psi = \psi^\dagger \gamma^0$. The gamma matrices $\gamma^\mu$ are defined to obey
\begin{equation}
\{\gamma^\mu,\gamma^\nu\} = 2 \eta^{\mu\nu}
\text.
\end{equation}
For our purposes, it is convenient to select $\gamma^0 = \sigma_3$ and $\gamma^1 = -i\sigma_1$; however, this choice is arbitrary. Note that $\gamma^0$ is Hermitian while $\gamma^1$ is anti-Hermitian.

\subsection{Naive fermions}

Naively discretizing the continuum Hamiltonian yields the following lattice Hamiltonian:
\begin{equation}\label{eq:naive}
H = \sum_x \bigg[\frac{\bar\psi_x \sigma_1 \psi_{x+1} - \bar\psi_{x+1} \sigma_1 \psi_x}{2} + m \bar\psi_x \psi_x\bigg]
\text.
\end{equation}
We will now show that this lattice Hamiltonian in fact describes two species of two-component fermions, rather than the desired one. This phenomenon is referred to as fermion doubling, and the purpose of staggered fermions is to remove (or at least alleviate) it.

Because the Hamiltonian is quadratic in the fields, it can be solved exactly by diagonalization. But for the spin structure, the Hamiltonian is diagonalized in momentum basis. Define the Fourier-transformed fields $\psi(p)$ so that
\begin{equation}
\psi_x = \frac 1 L \sum_p e^{ipx} \psi(p)
\text{ and }
\bar\psi_x = \frac 1 L \sum_p e^{-ipx} \bar\psi(p)
\text.
\end{equation}
Here the momentum sums are taken over all momenta of the form $\frac{2 \pi n}{L}$ for integer $n$. In the limit $L\rightarrow\infty$, these become the expected momentum integrals $\int \frac{dp}{2\pi}$:
\begin{equation}
\psi_x = \int \frac{dp}{2\pi} e^{ipx} \psi(p)
\text{ and }
\bar\psi_x = \int \frac{dp}{2\pi} e^{-ipx} \bar\psi(p)
\text.
\end{equation}

Substituting into the naive Hamiltonian (\ref{eq:naive}) reveals
\begin{equation}
H = \int \frac{dp}{2\pi} \bigg[i (\sin p)  \bar\psi(p) \sigma_1 \psi(p) + m \bar\psi(p) \psi(p)\bigg]
\text.
\end{equation}
In principle, we complete the diagonalization by introducing a new two-component spinor $u$ defined by $\psi = A u$, for some $2 \times 2$ matrix $A$ that diagonalizes the spin-mass matrix. It is sufficient for our purposes to note that the spin-mass matrix for the mode of momentum $p$ is
\begin{equation}
H_p = \psi^\dagger \left(
\begin{matrix}
m & -i \sin p\\
i \sin p & -m
\end{matrix}
\right)
\psi
\text,
\end{equation}
which has eigenvalues $\pm \sqrt{m^2 + \sin^2 p}$. The positive and negative energy modes correspond, as usual, to particles and antiparticles, and won't concern us here. However, at both $p = 0$ and $p = \pi$ we see that the usual relativistic dispersion relation is recovered. Modes near $p=0$ correspond to the expected continuum physics; those near $p=\pi$ are the undesirable doublers.

\subsection{Staggered fermions}

In principle, staggered fermions can be ``derived'' by spin-diagonalizing the Hamiltonian for naive fermions, and then dropping the unwanted component. To start with, we will instead simply guess a Hamiltonian, and then calculate its continuum behavior.

Let $\chi$ be a one-component fermionic field, obeying the usual anticommutation relations. Consider the lattice Hamiltonian
\begin{equation}\label{eq:stag-1d}
H = \sum_x (-1)^x \bigg[\frac{\chi^\dagger_{x+1} \chi_x + \chi^\dagger_x \chi_{x+1}}{2} + m (-1)^x \chi^\dagger_x \chi_x\bigg]
\text.
\end{equation}
It is easy to verify that this Hamiltonian is not diagonalized by a simple switch to momentum basis (there is mixing between the mode of momentum $p$ and that of $p+\pi$). Instead, we must define a modified momentum-space fermionic operator $u(p)$ via
\begin{equation}
\chi(x) = \int \frac{dp}{2\pi} e^{ipx}
\begin{cases}
u(p) & x\text{ even}\\
u^\dagger(p) &x \text{ odd}\text.
\end{cases}
\end{equation}
\todo wrong. 
The operator $u(p)$ in fact corresponds to the physical, continuum fermion of momentum $p$. Physically, this is saying that $\chi$ is an annihilation operator on even sites, but a creation operator on odd sites. Substituting into the lattice Hamiltonian (\ref{eq:stag-1d}) yields
\begin{equation}
H = \int \frac{dp}{2\pi} \bigg[\todo + m \chi^\dagger(p) \chi(p)\bigg]
\end{equation}

\todo In the diagonalizing basis, the number operator is $\int u^\dagger u$. Tracing the derivation back, it is easy to see that the corresponding lattice density operator is $n(x) = \chi^\dagger(x) \chi(x)$. This also meshes with the above physical interpretation of $\chi(x)$ at even/odd $x$.

Of course many other expressions of staggered fermions in one dimensional are possible, related to each other by changes of variables.

\subsection{Adding gauge fields}
The main insight to make adding gauge fields ``easy'' is that in the weak-coupling limit---or equivalent, in a flat gauge background---the original staggered Hamiltonian should be recovered. Each fermionic operator $\chi$ picks up a color index, and imposing local gauge invariance means that the interacting Hamiltonian must read
\begin{equation}
H = \sum_x (-1)^x \bigg[\frac{\chi^\dagger_{x+1} U_x \chi_x + \chi^\dagger_x U^\dagger_x \chi_{x+1}}{2} + m \chi^\dagger_x \chi_x\bigg]
\text.
\end{equation}
In the case of $SU(N)$ fields, this also means adding more fermionic components, and appropriate contractions between the fields $U$ and $\chi$.

\subsection{Spin-diagonalization}
As mentioned, the above analysis can be done in reverse, starting with the naive discretization (\ref{eq:naive}) and deriving the staggered Hamiltonian. This is accomplished by performing a space-dependent change of variables, defining a new two-component spinor $\chi$ via
\begin{equation}
\psi_x = \sigma_1^x \chi_x
\text.
\end{equation}
In terms of these new spinors $\chi$, the naive Hamiltonian now reads
\begin{equation}
H = \sum_x \bigg[(-1)^x\frac{\chi_x^\dagger \sigma_3 \chi_{x+1} - \chi_{x+1}^\dagger \sigma_3 \chi_x}{2} + m (-1)^x \chi_x^\dagger \sigma_3 \chi_x\bigg]
\text.
\end{equation}
Note that this Hamiltonian describes exactly the same system---nothing physical has been changed, but only the labelling of the spinors. In this form, however, it is clear that the first and second components of $\chi$ are uncoupled. We can therefore remove half the fermion species by considering only the first component of $\chi$. With $\chi$ a one-component spinor, the staggered Hamiltonian is
\begin{equation}
H = \sum_x \bigg[(-1)^x\frac{\chi_x^\dagger \chi_{x+1} - \chi_{x+1}^\dagger \chi_x}{2} + m (-1)^x \chi_x^\dagger \chi_x\bigg]
\end{equation}
as seen before.

\section{Three dimensions}

In this section we consider staggered fermions in three spatial dimensions. The target systems typically have the form 
\begin{equation}\label{eq:naive-3d}
\mathcal H =
\sum_f \bar\psi_f (i \gamma^i\partial_i + m_f - \mu \gamma^0) \psi_f
\text,
\end{equation}
where the sum is taken over $N_f$ flavors of fermions, and $m_f$ is a flavor-dependent mass. A simple but already interesting case is $N_f=2$ and $m_f=0$, yielding the chiral limit of QCD (in the absence of strange quarks). This is expected to be informative with respect to the high-density equation of state. An improved approximation would add a strange quark of non-vanishing mass. Unfortunately, such a system is not accessible with staggered fermions in the Hamiltonian formulation\footnote{In the context of lattice path integrals, a single fermion species is traditionally obtained by taking the fourth root of a fermion determinant that would otherwise describe four flavors.}.

\subsection{Gamma matrices}
As before, the Gamma matrices are defined to anticommute, and are normalized such that $\gamma^\mu\gamma^\mu = \eta^{\mu\mu}$. The conventional choice of representation---the Dirac representation---is
\begin{equation}
\gamma^0 = \sigma_z \otimes I
\text{ and }
\gamma^i = i \sigma_y \otimes \sigma_i
\text.
\end{equation}
The two other notable bases are the Weyl basis
\begin{equation}
\gamma^0 = \sigma_x \otimes I
\text{ and }
\gamma^i = i \sigma_y \otimes \sigma_i
\text,
\end{equation}
and the Majorana basis
\begin{equation}
\gamma^0 = \sigma_x \otimes \sigma_y
\text{, }
\gamma^1 = i I \otimes \sigma_z
\text{, }
\gamma^2 = -i \sigma_y \otimes \sigma_y
\text{, and }
\gamma^3 = -i I \otimes \sigma_x
\text.
\end{equation}

\subsection{Spin-diagonalization}
We begin with the naive discretization of the free-fermion Hamiltonian (\ref{eq:naive-3d}):
\begin{equation}
H = \sum_x \Bigg[
\sum_i
\frac{
\bar\psi_{x+\hat i}
\gamma^i 
\psi_x
-
\bar\psi_x
\gamma^i 
\psi_{x+\hat i}
}{2}
+
m \bar\psi_x \psi_x - \mu \bar\psi_x \gamma^0 \psi_x
\Bigg]
\text.
\end{equation}
Here $\psi$ is a single four-component spinor. The outer sum is taken over all lattice sites $x$, and in inner sum over the three directions. The extra species of fermion will appear as a lattice doubler, and we are ignoring color for the time being.

The spin-diagonalization is accomplished by defining a new four-component spinor $\chi$ according to
\begin{equation}
\psi_x = (\gamma^1)^{x_1}(\gamma^2)^{x_2}(\gamma^3)^{x_3}\chi_x
\text.
\end{equation}
Since $(\gamma^i)^\dagger (\gamma^i) = 1$, the chemical potential term is unaffected by this transformation. The mass term is affected by $x_1+x_2+x_3$ commutations of matrices $\gamma^i$ through the $\gamma^0$, and therefore acquires an overall sign of $(-1)^{x_1+x_2+x_3}$. Both terms began as diagonal, and remain diagonal.

The kinetic term requires more care, but it is readily seen that the term remain diagonal, having the form $\chi^\dagger \gamma^0 \chi$. Factors of $(-1)$ appear for every commutation that must be performed on the way to getting all the $\gamma_i$ paired off. Considering the derivative along axis $2$ as a representative example, note that any factors of $\gamma^1$ on the right may be commuted past the $\gamma^0 \gamma^1$ for free. However, factors of $\gamma^2$ and $\gamma^3$ each contribute a sign, as they must be commuted only past a single gamma matrix. Putting this all together, the same system, written in terms of $\chi$ instead of $\psi$, has Hamiltonian
\begin{equation}
H =
\sum_x \bigg[
\sum_i \eta_i(x) \frac{\chi^\dagger_{x+\hat i} \gamma^0 \chi_x - \chi^\dagger_x \gamma^0 \chi_{x+\hat i}}{2}
+ m (-1)^{x} \chi^\dagger \gamma^0 \chi
- \mu \chi^\dagger \chi
\bigg]
\text,
\end{equation}
where the staggered factor is defined as $\eta_i(x) \equiv \prod_{j\ge i}(-1)^{x_j}$, and $(-1)^x$ is shorthand for $\prod_i (-1)^{x_i}$.

Dropping, as before, all but the first component of the spinor $\chi$, the staggered Hamiltonian is found to be
\begin{equation}
H =
\sum_x \bigg[
\sum_i \eta_i(x) \frac{\chi^\dagger_{x+\hat i} \chi_x - \chi^\dagger_x \chi_{x+\hat i}}{2}
+ m (-1)^{x} \chi^\dagger \chi
- \mu \chi^\dagger \chi
\bigg]
\text.
\end{equation}
\end{document}
